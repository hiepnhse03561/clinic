<?php

namespace App\Models;

class Appointment extends BaseModel
{
    protected $table = 'appointment';

    protected $fillable = [
        'patient_id', 'doctor_id', 'shift_id', 'date', 'status_id', 'description'
    ];
}
