<?php

namespace App\Models;

class Status extends BaseModel
{
    protected $table = 'status';

    protected $fillable = [
        'name'
    ];
}
