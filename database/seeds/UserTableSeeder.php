<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserRole;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRoles = UserRole::where('id', '>', 1)->pluck('id')->toArray();
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        User::create([
            'name' => 'Nguyen Huy Vinh',
            'email' => 'vinhnh.uet.195@gmail.com',
            'password' => bcrypt('123456789'),
            'phone_number' => '01649250269',
            'address' => 'Thai Binh',
            'role_id' => 2,
        ]);

        User::create([
            'name' => 'Nguyen Van Tranh',
            'email' => 'tranhnv@gmail.com',
            'password' => bcrypt('123456789'),
            'phone_number' => '01649250269',
            'address' => 'Thai Binh',
            'role_id' => 3,
        ]);

        User::create([
            'name' => 'Vu Minh Tuan',
            'email' => 'tuanvm@gmail.com',
            'password' => bcrypt('123456789'),
            'phone_number' => '01649250269',
            'address' => 'Thai Binh',
            'role_id' => $userRoles[array_rand($userRoles)],
        ]);

        User::create([
            'name' => 'Do Hoa An',
            'email' => 'andh@gmail.com',
            'password' => bcrypt('123456789'),
            'phone_number' => '01649250269',
            'address' => 'Thai Binh',
            'role_id' => $userRoles[array_rand($userRoles)],
        ]);
    }
}
