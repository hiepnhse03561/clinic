<?php

use Illuminate\Database\Seeder;
use App\Models\Shift;
use Carbon\Carbon;

class ShiftTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Shift::truncate();
        Schema::enableForeignKeyConstraints();

        Shift::create([
            'startTime' => Carbon::parse('07:00:00', 'Asia/Ho_Chi_Minh'),
            'endTime' => Carbon::parse('08:50:00', 'Asia/Ho_Chi_Minh'),
        ]);

        Shift::create([
            'startTime' => Carbon::parse('09:00:00', 'Asia/Ho_Chi_Minh'),
            'endTime' => Carbon::parse('10:50:00', 'Asia/Ho_Chi_Minh'),
        ]);

        Shift::create([
            'startTime' => Carbon::parse('13:00:00', 'Asia/Ho_Chi_Minh'),
            'endTime' => Carbon::parse('14:50:00', 'Asia/Ho_Chi_Minh'),
        ]);

        Shift::create([
            'startTime' => Carbon::parse('15:00:00', 'Asia/Ho_Chi_Minh'),
            'endTime' => Carbon::parse('16:50:00', 'Asia/Ho_Chi_Minh'),
        ]);
    }
}
