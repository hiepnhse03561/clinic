<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_id')->unsigned();
            $table->integer('patient_id')->unsigned();
            $table->integer('appointment_id')->unsigned();
            $table->double('amount');
            $table->dateTime('payment_date');
            $table->timestamps();
            $table->foreign('status_id')->references('id')->on('status');
            $table->foreign('patient_id')->references('id')->on('users');
            $table->foreign('appointment_id')->references('id')->on('appointment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice');
    }
}
